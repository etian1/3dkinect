%% Get green laser dots using Sift.
% This function takes in an image file and calculates SIFT keypoints
% and descriptors using the vl_sift package
% (http://www.vlfeat.org/overview/sift.html). The results are thresholded
% with a peak selection threshold PeakThresh, non-edge selection threshold
% EdgeThresh, a maximum pixel radius maxRad, and green amount greenThresh.
% The number of keypoints found and the time taken for the code to run are
% outputted to the command line, along with additional information
% about the SIFT process.

function[f, d] = getDotsSIFT(image)

%% Start timing
tic;

    %% Set up the vl_sift package.
    run('../vlfeat-0.9.20/toolbox/vl_setup')
    close all;
    
    %% Process the input image.
    I = imread(image);          % read image
    Ig = rgb2gray(I);           % convert to greyscale
    Ig = single(Ig);            % convert to class single
    
    %% Find SIFT features and threshold.
    % use vl_sift to find features
    [f, d] = vl_sift(Ig, 'PeakThresh', 4, 'EdgeThresh', 30, 'Verbose');
    
    % threshold by green amount
    greenThresh = 60;               % needs to be smaller for dark scenes and vice versa
    for i=1:size(f, 2)
        y = int16(f(1, i));
        x = int16(f(2, i));
        if I(x, y, 2) < greenThresh
            f(:, i) = zeros(4, 1);  % delete the feature if not green enough
        end
    end
    
    % threshold size relatively
    maxRad = max(f(3, :));  % maximum radius
    %threshRel = 0.3;           % keep bottom 30%
    %k = find((f(3, :)<=threshRel*maxRad) & (f(3, :)>0));
    
    % threshold size absolutely
    threshAbs = 7;             % pixels
    k = find((f(3, :)<=threshAbs & (f(3, :)>0)));
    f = f(:, k);                % truncate f

    %% Plot SIFT features.
    % adapted from vl_demo_Sift_basic.m
    % feat = 100;                 % generate random set of features to display
    feat = size(f, 2);          % display all features
    figure();
    imshow(I);
    axis equal; axis off; axis tight;
    hold on;
    perm = randperm(size(f, 2));  
    sel  = perm(1:feat);            % generate selection of features
    h1   = vl_plotframe(f(:,sel)) ; set(h1,'color','y','linewidth',2);
    
    %{
    %% Perform k means clustering on the results
    % not thoroughly tested; may be effective if we can limit to center region?
    [labels, centers] = kmeans(f',800);
    figure();
    imshow(I2);
    axis equal; axis off; axis tight;
    hold on;
    h1   = vl_plotframe(centers') ; set(h1,'color','k','linewidth',3);
    h2   = vl_plotframe(centers') ; set(h2,'color','y','linewidth',2);
    %}
    
%% Stop timing
toc;

end
