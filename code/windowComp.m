%% Return similarity between windows in two images
% This script computes and returns the "similarity" in terms of the
% specified metric (ncc for normalized cross correlation or ssd for sum of
% squared differences) between a window in image1 centered at center1 with
% radius windowRad and a window in image2 centered at center2 with radius
% windowRad. If the window does not fit within either image, a value of 0
% will be returned as the windows can't be compared.

function [similarity] = windowComp(img1, img2, center1, center2, windowRad, metric)
%% Get image size
imgSize = size(img1);
xMaxVal = imgSize(2);
yMaxVal = imgSize(1);

%% Calculate window ranges
xMin1 = round(center1(1) - windowRad);
xMax1 = round(center1(1) + windowRad);
yMin1 = round(center1(2) - windowRad);
yMax1 = round(center1(2) + windowRad);

xMin2 = round(center2(1) - windowRad);
xMax2 = round(center2(1) + windowRad);
yMin2 = round(center2(2) - windowRad);
yMax2 = round(center2(2) + windowRad);

%% Compute similarity
% If window out of range, return 0
if ((xMin1 < 1) || (xMin2 < 1) || (yMin1 < 1) || (yMin2 < 1) || ...
        (xMax1 > xMaxVal) || (xMax2 > xMaxVal) || ...
        (yMax1 > yMaxVal) || (yMax2 > yMaxVal))
    similarity = 0;
% If window within range, return similarity
else
    % Get green channel of each image in window
    mat1 = img1(yMin1:yMax1,xMin1:xMax1,2);
    mat2 = img2(yMin2:yMax2,xMin2:xMax2,2);
    
    % Compute similarity as normalized cross correlation
    if (strcmp(metric, 'ncc'))
        similarity = norm(normxcorr2(mat1, mat2));
    % Compute similarity as sum of squared differences
    elseif (strcmp(metric, 'ssd'))
        diff = mat1 - mat2;
        similarity = sum(sum(diff.^2));
    end
end