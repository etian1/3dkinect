%% Find laser pattern dot center coordinates using MSER.
% This function takes in an RGB image, and T/F parameters. When findCenter
% is true the function will make the first center point that of the dot at
% the center of the laser pattern. When eliminateDup is true, no two dots
% that center points within minInterDotDist of each other will be included
% in the returned list. When returnPixelLists is true the function will
% also return a list of the pixel coordinates for each pixel within the dot
% for each dot found. 
%
% This function returns a matrix with the list of centers, with each row
% storing the (x,y) coordinates of the center of the dot/region. The
% variable pixelLists is in the same order as centers and has the
% coordinates of all the pixels for each dot/region.
function[centers, pixelLists] = getDots(I, findCenter, eliminateDup, returnPixelLists)

%% Parameters to set
% Max eccentricity for a region (to filter out non-circular dots)
maxEccentricity = 1;

% Bounds on region area to be counted as a normal dot in the laser pattern
minArea = 0;
maxArea = 100;

% Minimum distance between dots allowed if duplicates are to be eliminated
minInterDotDist = 10;

%% Get MSER features
Ig = rgb2gray(I);
[regions, mserCC] = detectMSERFeatures(Ig);

%% Filter MSER features by eccentricity and size

% Get information about each of the regions
stats = regionprops('table',mserCC,'Eccentricity');
statsSize = regionprops('table',mserCC,'FilledArea');

% Define bounds on eccentricity and size
eccentricityIdx = stats.Eccentricity < maxEccentricity;
sizeIdx1 = statsSize.FilledArea > minArea;
sizeIdx2 = statsSize.FilledArea < maxArea;

% Filter by bounds defined above
circularRegions = regions(eccentricityIdx);
circularSmall1Regions = circularRegions(sizeIdx1);
circularSmallRegions = circularSmall1Regions(sizeIdx2);

% Comment in the lines below to display count of MSER regions
% disp('Number of MSER regions:');
% disp(circularSmallRegions.Count);

% Get pixel lists for each dot if needed
pixelLists = 0;
if (returnPixelLists)
    pixelLists = circularSmallRegions.PixelList;
end

%% Handle center point of laser pattern
% Accounts for maybe needing to add center of laser pattern as first dot in list centers
offset = 0;     

if (findCenter)
    
    % Find brightest points within grayscale image
    grayBright = Ig >= 255;
    brightIdxs = find(grayBright(:));
    brightCoords = zeros(size(brightIdxs,1),2);
    for i=1:size(brightIdxs,1)
        [r,c] = ind2sub(size(grayBright),brightIdxs(i)); 
        brightCoords(i,1) = c;    % switch c and r to match image coords
        brightCoords(i,2) = r;
    end
    
    % Find center of laser pattern as centroid of brightest points and add
    % this point as the first "dot" in the list of dots in the laser
    % pattern.
    centers(1,1) = mean(brightCoords(:,1));
    centers(1,2) = mean(brightCoords(:,2));
    
    % Set offset to 1 so that later dots have apropriate indices
    offset = 1;
    
    % Comment in to plot center of laser pattern to check correctness
%     plot(mean(brightCoords(:,1)),mean(brightCoords(:,2)),'r.','MarkerSize',20);

    % If need to return pixel list, only pixel for center of laser pattern
    % is the pixel that is actually the center, add this at the front of
    % the lists of pixels.
    if(returnPixelLists)
        centerPixels = cell(1,1);
        centerPixels{1} = centers(1,:);
        pixelLists = vertcat(centerPixels,pixelLists);
    end

end

% Populate centers with center coordinates, accounting for offset due to
% including center point of laser pattern if needed.
for i=1:circularSmallRegions.Count
    centers(i+offset,1) = mean(circularSmallRegions.PixelList{i,1}(:,1));
    centers(i+offset,2) = mean(circularSmallRegions.PixelList{i,1}(:,2));
end

% Eliminate dots that are too close to each other if necessary
if (eliminateDup)
    % To store indices of dots to keep
    keepIdxs = [1];
    
    % Go through all dots after first dot to check if too close to a
    % previously found dot 
    for centersIdx=2:circularSmallRegions.Count
        foundDup = false;
        
        % Iterate backwards through prior dots because regions returned by
        % MSER are in order by location so nearby dots are adjacent to each
        % other in the list of dots returned by MSER
        for earlierCenterIdx=(centersIdx-1):-1:1 %max(1,centersIdx-1)
            delta = centers(centersIdx,:)-centers(earlierCenterIdx,:);
            dist = sqrt(sum(delta.^2));
            if (dist < minInterDotDist)
                foundDup = true;
                break;
            end
        end
         
        % If no duplicate found, keep dot
        if (~foundDup)
            keepIdxs = [keepIdxs,centersIdx];
        end
    end  
    
    % Filter out duplicates from list of centers
    centers = centers(keepIdxs,:);
    
    % Filter out duplicates from pixel lists if needed
    if(returnPixelLists)
        pixelLists = pixelLists(keepIdxs);
    end
    
end

%% Plot MSER regions
% Comment in the section below to show MSER regions overlayed on image
% figure;
% imshow(I);
% hold on;
% disp(size(circularSmallRegions))
% disp(keepIdxs)
% plot(circularSmallRegions(keepIdxs), 'showPixelList', true, 'showEllipses', false);


% Comment out if don't want to see number of centers found in image
disp('Number of centers in image:');
disp(size(centers,1));

end