%%Description
% This script uses data from training.m and buildLookupTable.m about the
% relationship between (x,y) pixel coordinates and distance for each dot in
% the laser pattern from the training data to produce a 3D point cloud
% based on a new image. Dots in the new input image are matched to dots in
% the template training image based on their location within the image
% (based on which dots could potentially be in that location). This is done
% using the lookupTable matrix produced by buildLookupTable.m. Distance
% is calculated based on xDistLines produced by buildLookupTable.m

%% Choose data set
% dataSet = 'Old';
dataSet = 'New';

%% Parameters to set
% true if testing on one of the training images just to check, false
% otherwise.
trainingImg = true;

if (trainingImg)
    % Choose which training image to use for testing -- make sure it
    % matches with training data set that is being used!
    actualDist = 161;
    testImgName = '../training2/161mm.ppm';
else
    % Choose testing image to use -- make sure it matches with training
    % data set that is being used!
    testImgName = '../testing2/stepstraight13_2017-05-10-112120-0000_undistorted.ppm';
end

% Radius of area to search for line representing a laser pattern dot near
% the (x,y) coordinates of the center of a dot in a new image
rad = 1;

% Radius of window around dot to compare with closest distance training
% image
windowRad = 30;

% Minimum normalized cross correlation between windows around dot in new
% image and window around dot in closest distance training image
nccThresh = 5;

% Only use point if only one possible matching dot (true) or find best dot
% match if multiple matching dots (false)
max1Line = true;

% Maximum difference from actual distance that can count as good match
goodThresh = 5;

%% Camera parameters from calibration
% Focal length
fc = [ 711.110239283483679 ; 705.913215862433731 ];

% Principal point
cc = [ 644.571322395686366 ; 492.894926761113595 ];

% Distance in mm between front of camera holder and camera center based on
% calibration and known distances for some calibration images
camDepth = 11;

%% Set things appropriately based on data set being used
if (dataSet == 'Old')
    knownDistsDir = '../041317knownDistanceImages/';
    lowerBoundDist = 131;
    upperBoundDist = 201;
elseif (dataSet == 'New')
    knownDistsDir = '../051017/';
    lowerBoundDist = 61;
    upperBoundDist = 161;
end

% Check that distance in appropriate range
if (trainingImg)
    assert((actualDist >= lowerBoundDist) && (actualDist <= upperBoundDist))
end

%% Load lookupTable and relation between x pixel coordinate and distance
clear xDistLines; clear imgNames; clear lookupTable; clear trainingDots; clear dists;
load(strcat('lines', dataSet, '.mat'));

%% Create depth map
% Read in new image and get dot coordinates
testImg = imread(testImgName);
imgSize = size(testImg);
[centersTest,~] = getDots(testImg, false, true, false);

%% Set up variables for building depth map
% To store 3D points generated
points = zeros(0,3);

% To store (x,y) pixel coordinates of dots that are used to generate 3D
% points
plottedPoints = zeros(0,2);

% Storing max normalized cross correlation values for each dot to assess
% what is the best threshold to use.
nccMaxs = [];

% Count of good matches and bad matches based on actual distance
good = 0;
bad = 0;

%% Put together data to make depth map
for dotIdx=1:size(centersTest,1)
    
    %% Find all potential dot matches
    % Get coordinates of center of dot in new image
    center = centersTest(dotIdx,:);
    x = center(1);
    xRound = round(x);  % Need rounded values to index into matrices
    y = center(2);
    yRound = round(y);

    % To store list of dot indices for dots that have lines going near the
    % pixel coordinates of the center of current dot in new image
    closeLines = zeros(0);
    
    % Window around dot to check for all possible line matches
    minX = xRound - rad;
    maxX = xRound + rad;
    minY = yRound - rad;
    maxY = yRound + rad;
    
    % If window around dot is within image, add dot index to list of close
    % lines
    if ((minX >= 1)&&(minY >= 1)&&(maxX <= imgSize(1))&&(maxY <= imgSize(2)))
        for i=-rad:1:rad   % change this later to leave more flexibility for x than for y
            for j=-rad:1:rad
                closeLines = [closeLines,lookupTable{xRound+i,yRound+j,:}];
            end
        end
    end
    
    % Eliminate duplicates in closeLines
    closeLines = unique(closeLines);

    %% Validate match (or find best match among potential matches)
    
    % If only include 3D point based on dot if there is only 1 possible
    % match found, just use that match if it exists and if there are
    % multiple matches don't generate a 3D point based on that dot.
    if (max1Line)
        nccMax = nccThresh;
        foundMatch = (length(closeLines) == 1);
        if (foundMatch)
            lineMatch = closeLines(1);
        end
    
    % If allowed to choose between multiple potential matches, compare
    % windows around dot between images
    else
        % Parameters to keep track of best match amongst lines
        bestMatch = 0;
        nccMax = -100000000000;
        for i=1:length(closeLines)
            % Get distance estimate based on potential match
            dotLineMatch = closeLines(i);
            distEst = polyval(xDistLines(dotLineMatch,:), x);

            % Round distance estimate to find training image with closest
            % distance to compare window
            if (dataSet == 'Old')
                roundedDistEst = round(distEst);
            elseif (dataSet == 'New')
                distEstOff = distEst - camDepth;
                distEstOffScaled = distEstOff / 5.0;
                roundedDistEstOffScaled = round(distEstOffScaled);
                roundedDistEstOff = 5.0 * roundedDistEstOffScaled;
                roundedDistEst = roundedDistEstOff + camDepth;
            end
            
            imgEstName = strcat(knownDistsDir, sprintf('%03d', roundedDistEst), 'mm.ppm');
            imgEst = imread(imgEstName);
            roundedDistEstIdx = find(dists == roundedDistEst);

            ncc = windowComp(testImg, imgEst, [xRound, yRound],trainingDots(roundedDistEstIdx,dotLineMatch,:),windowRad,'ncc');
            if (ncc)
                if (ncc > nccMax)
                    nccMax = ncc;
                    bestMatch = closeLines(i);
                end 
            else
                disp(xRound);
                disp(yRound)
            end
        end

        % Finalize match
        foundMatch = false;
        lineMatch = 0;
        if (bestMatch)   % If found at least one matching line
            nccMaxs = [nccMaxs, nccMax];
            if (nccMax > nccThresh) % If window matching good enough
                foundMatch = true;
                lineMatch = bestMatch;
            end
        end
    end
      
    %% Add to plot if found match
    if(foundMatch)
        % Compute distance based on polynomial relating x pixel coordinate
        % and distance for matched dot
        dist = polyval(xDistLines(lineMatch,:), x);
        
        % Compute 3D coordinates by doing projection based on camera
        % parameters
        zReal = dist;
        xReal = zReal*(xRound-cc(1))/fc(1);
        yReal = zReal*(yRound-cc(2))/fc(2);
        
        % For debuggingevaluation purposes, keep track of number of good 
        % and bad dot matches
        if (trainingImg)
            if (abs(zReal - actualDist) > goodThresh)
                bad = bad + 1;
%                 disp('[xRound,yRound,zReal]');
%                 disp([xRound,yRound,zReal]);
%                 disp(nccMax)
%                 disp(length(closeLines));
            else
%                 disp(length(closeLines));
                good = good + 1;
            end
        end
        
        % Add 3D points to list of points to be plotted
        points = [points; xReal, yReal, zReal];
        
        % Add 2D pixel coordinates to list of points to be plotted to be
        % depicted on original image to show which dots gave 3D points
        plottedPoints = [plottedPoints; xRound, yRound];
    end
end

%% Plot computed 3D points with plane fit to points
figure;
offset = 150;
fittedObject = fit([points(:,1),points(:,2)]+offset,points(:,3), 'poly21');
plot(fittedObject,[points(:,1),points(:,2)]+offset,points(:,3))
xlabel(strcat('X+',int2str(offset), ' (mm)'));
ylabel(strcat('Y+',int2str(offset), ' (mm)'));
zlabel('Z (mm)');

%% Plot just x location vs. distance to assess accuracy
figure;
scatter(points(:,1),points(:,3))
xlabel('X (mm)');
ylabel('Z (mm)');

%% Draw dots that gave 3D points on original image 
figure;
imshow(testImg);
hold on;
plot(plottedPoints(:,1),plottedPoints(:,2),'r.','MarkerSize',5);

%% Print how many good and how many total points were computed
% To assess performance
disp(good)
disp(bad + good);