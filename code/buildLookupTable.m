%% Description
% This script populates the matrices lookupTable and xDistLines based on
% the results from the script training.m about the locations of each dot
% in the laser pattern in each image (stored in trainingDots). The matrix
% lookupTable has rows representing x pixel coordinate, columns
% representing y pixel coordinate, and each entry is a list of dot indices
% for dots that are centered at that (x,y) pixel coordinate at some
% distance. The matrix xDistLines stores the coefficients of the polynomial
% fit to the relationship between x pixel coordinate and distance.Then

%% Parameters to set
% Index of shortest distance to include in lookupTable, use 1 to include
% all distances
startDistIdx = 1;  

% Order of polynomial to fit to (x,y) coordinates for dot in laser pattern
% as distance varies
polyOrder = 2;

% Dots will only be in lookupTable if they have normalized residuals
% summing to below this value. 
fitThresh = 5;

% Type of plot to display (2 for 2D plot of (x,y) pixel coordinates or 3 
% for 3D plot of (x,y,distance))
plotDim = 2;

% Whether to plot fitted lines (true) or original points (false)
plotFit = true;

%% Choose data set:
dataSet = 'Old';
% dataSet = 'New';

%% Load training image dot coordinates
clear trainingDots; clear dists; clear imgNames;
load(strcat('trainingDots', dataSet, '.mat'));   % CHANGE LINES MAT FILE NAME AT END TOO

%% Set up variables for building lookupTable
% To store coordinates of center of current dot
center = zeros(1,2); 

% To store coefficients of polynomial fit to relationship between x and y
% pixel coordinates for cetner of each dot in pattern as distance varies. 
xyLines = zeros(size(trainingDots,2),polyOrder+1);

% To store coefficients of polynomial fit to relationship between x pixel
% coordinate for dot center and distance of actual point 
xDistLines = zeros(size(trainingDots,2),polyOrder+1);

% To store 2D map with one line representing each dot in laser pattern, and
% each line having (x,y) coordinates of that dot at different distances.
% Lines are polynomials fit to data points.
lookupTable = cell(size(imread(imgNames(1,:))));

% Storing fit normalized residuals for all dots to assess what threshold
% best
fits = [];

% Count of dots included in lookupTable
count = 0;

% Create figure to display results in lookupTable
figure;

%% Plot center coordinates relative to distance
for dotIdx=1:size(trainingDots,2)
    
    % To store all (x,y) pixel coordinates of dots across images at varying
    % distances
    xVals = zeros(size(dists,2),1);
    yVals = zeros(size(dists,2),1);
    
    % Iterate through images
    for imageIdx=1:size(xVals,1)
        
        % Get dot center pixel coordinates
        center(:) = trainingDots(imageIdx,dotIdx,:);
        
        % Add dot center pixel coordinates to list if there is a match
        if ((0==center(1)) && (0==center(2)))
            xVals(imageIdx) = 0;
            yVals(imageIdx) = 0;
        else
            xVals(imageIdx) = center(1);
            yVals(imageIdx) = center(2);
        end
    end
    
    % Only keep (x,y) pixel coordinates for dots that were found
    isThere = find(xVals);
    xVals = xVals(isThere);
    yVals = yVals(isThere);
    
    % Only keep distances for which image contained current dot
    currDists = dists(isThere);
    
    % If dot was found in at least 4 images, include in lookupTable
    if(length(xVals) >= 4)
        
        % Fit polynomial to (x,y) dot center pixel coordinates
        % xyLines will store polynomial coefficients, S_y stores info about
        % fit quality
        [xyLines(dotIdx,:),S_y] = polyfit(xVals, yVals, polyOrder);
        
        % Get quality of fit of points to polynomial (sum of normalized
        % residuals)
        fitCurr = S_y.normr;
        fits = [fits;fitCurr];

        % If polynomial fit was good
        if (fitCurr < fitThresh)
            cound = count + 1;     % keep track of number of good fits
            
            % Get x pixel coordinates ranging from min to max values
            % actually found in images
            fittedX = max(1,floor(min(xVals))):1:min(size(lookupTable,1),ceil(max(xVals)));
            
            % Get y pixel coordinates based on polynomial fit to (x,y)
            % relationship
            fittedY = round(polyval(xyLines(dotIdx,:), fittedX));

            % For each possible integer x pixel coordinate, add entry to
            % lookupTable for what the corresponding y pixel coordinate is.
            % The lookupTable will store a list of the indices of the lines
            % that go through each pixel.
            for i=1:size(fittedX,2)
                lookupTable{fittedX(i),fittedY(i)} = [lookupTable{fittedX(i),fittedY(i)},dotIdx];
            end

            % Fit polynomial to relation between x pixel coordinate and 
            % known image distances, S_dist stores info about fit quality.
            [xDistLines(dotIdx,:),S_dist] = polyfit(xVals,currDists', polyOrder);
            
            % Get distance values based on polynomial fit to relationship
            % between x pixel coordinate and distance.
            fittedDists = round(polyval(xDistLines(dotIdx,:), fittedX));

            %% Add line for current laser pattern dot to lookupTable plot
            if(plotDim == 2)
                if (plotFit)
                    plot(fittedX, fittedY);
                else
                    plot(xVals,yVals);
                end
                hold on;
            elseif(plotDim == 3)
                if (plotFit)
                    plot3(fittedX, fittedY, fittedDists);
                else
                    plot3(xVals, yVals, currDists);
                end
                hold on;
            end

        end
    end
    
end

% Add labels to plot
xlabel('X Coordinate (pixels)');
ylabel('Y Coordinate (pixels)');
if (plotDim == 3)
    zlabel('Distance (mm)');
end

%% Save lookupTable and relation between x pixel coordinate and distance
save(strcat('lines', dataSet, '.mat'),'xDistLines','imgNames','lookupTable','trainingDots','dists');