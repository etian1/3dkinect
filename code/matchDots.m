%% Match dots in laser pattern between images
% This function takes in two images and the coordinates of the center
% points of the dots in the laser pattern in each of these images. The dots
% may be in different orders.
%
% This function returns a list of matches between dots in the test and
% template images. Each row of tmplMatches represents a match with the dot
% stored in the same row number in centersTmpl. The value stored within a
% given row in tmplMatches represents the index of the dot in the test
% image (within centersTest) that was matched to the dot in the template
% image. 
function[tmplMatches] = matchDots(imgTmpl, imgTest, centersTmpl, centersTest)

%% Parameters to set
% Radius of window around dot for comparing dots between images
windowRad = 30;

% Maximum distance from expected location for dot (expected location is
% based on translation of center point of laser pattern) allowed for match
maxDist = 10;

%% Find translation of center of laser pattern between images
% Center of laser pattern is first center in each list
patternCenterTmpl = centersTmpl(1,:);
patternCenterTest = centersTest(1,:);

% Find translation
xDiff = patternCenterTmpl(1) - patternCenterTest(1);
yDiff = patternCenterTmpl(2) - patternCenterTest(2);

%% Find best match in test image for each dot in template image
% Initially have no matches so all zeros
tmplMatches = zeros(size(centersTmpl,1),1);

% Auto-populate match between center of laser pattern between images
tmplMatches(1) = 1;

% Go through each dot in template image
for i=2:size(centersTmpl,1) % i = template center idx
    
    % Get coordinates of dot in template image
    tmplCenterX = round(centersTmpl(i,1));
    tmplCenterY = round(centersTmpl(i,2));
    
    % Find best match by normalized cross correlation between windows
    bestMatch = 0;
    nccMax = -1000000000000;
    
    % Compute expected location of dot based on assumption that all dots
    % translated by around the same amount as the center of the laser
    % pattern
    expectedLoc = centersTmpl(i,:) - [xDiff, yDiff];
    
    % Go through each dot in test image
    for j=2:size(centersTest,1)    % j = test center idx
        % Find distance between coordinates and expected location
        delta = centersTest(j,:)-expectedLoc;
        dist = sqrt(sum(delta.*delta));
        
        % If close enough to expected location
        if (dist < maxDist)          
            % Get coordinates of center of dot in test image
            testCenterX = round(centersTest(j,1));
            testCenterY = round(centersTest(j,2));
            
            ncc = windowComp(imgTest, imgTmpl, round(centersTest(j,:)), round(centersTmpl(i,:)), windowRad, 'ncc');
            if (ncc)
                if (ncc > nccMax)
                    nccMax = ncc;
                    bestMatch = j;
                end
            end
        end
    end
    
    % Initially indicate the best possible match of a dot in the test image
    % for the current dot in the template image
    tmplMatches(i) = bestMatch;
end

%% Find best match in template image for each dot in test image
% Initially have no matches so all zeros
testMatches = zeros(size(centersTest,1),1);

% Go through each dot in test image
for i=2:size(centersTest,1)
    
    % Get coordinates of dot in test image
    testCenterX = round(centersTest(i,1));
    testCenterY = round(centersTest(i,2));
    
    % Find best match by normalized cross correlation between windows
    bestMatch = 0;
    nccMax = -1000000000000;
    
    % Compute expected location of dot based on assumption that all dots
    % translated by around the same amount as the center of the laser
    % pattern
    expectedLoc = centersTest(i,:) + [xDiff, yDiff];
    
    % Go through each dot in template image
    for j=2:size(centersTmpl,1)
        % Find distance between coordinates and expected location
        delta = centersTmpl(j,:)-expectedLoc;
        dist = sqrt(sum(delta.*delta));
        
        % If close enough to expected location
        if (dist < maxDist)
            % Get coordinates of center of dot in template image
            tmplCenterX = round(centersTmpl(j,1));
            tmplCenterY = round(centersTmpl(j,2));
            
            ncc = windowComp(imgTest, imgTmpl, round(centersTest(i,:)), round(centersTmpl(j,:)), windowRad, 'ncc');
            if (ncc)
                if (ncc > nccMax)
                    nccMax = ncc;
                    bestMatch = j;
                end
            end

%             % Only do NCC if windows fit in image
%             if(~isnan(tmplCenterX) && ~isnan(tmplCenterY) && ...
%                     ~isnan(testCenterX) && ~isnan(testCenterY) && ...
%                     (tmplCenterX > windowRad) && ...
%                     (tmplCenterY > windowRad) && ...
%                     (tmplCenterX < numRows - windowRad) && ...
%                     (tmplCenterY < numCols - windowRad) && ...
%                     (testCenterX > windowRad) && ...
%                     (testCenterY > windowRad) && ...
%                     (testCenterX < numRows - windowRad) && ...
%                     (testCenterY < numCols - windowRad) )
%                 
%                 % Compute normalized cross correlation
%                 ncc = norm(normxcorr2(imgTmpl(tmplCenterX-windowRad:tmplCenterX+windowRad,...
%                     tmplCenterY-windowRad:tmplCenterY+windowRad,2),...
%                     imgTest(testCenterX-windowRad:testCenterX+windowRad,...
%                     testCenterY-windowRad:testCenterY+windowRad,2)));
%             
%                 % If found dot with best NCC so far, update bestMatch
%                 if (ncc > nccMax)
%                     nccMax = ncc;
%                     bestMatch = j;
%                 end
%             end
        end
    end
    
    % Initially indicate the best possible match of a dot in the template
    % image for the current dot in the test image
    testMatches(i) = bestMatch;
end

%% Check that dots are both best match for each other
for i=2:length(tmplMatches)
    if (tmplMatches(i) ~= 0)
        if (testMatches(tmplMatches(i)) ~= i)
            tmplMatches(i) = 0;
            % Comment in the lines below to print matched coordinates
%         else
%             disp('Test center coordinates:')
%             disp(centersTest(tmplMatches(i),:));
%             disp('Template center coordinates:');
%             disp(centersTmpl(i,:));
        end
    end
end

% Comment out if don't want to see number of matched dots between images
disp('Number of matches: ');
disp(sum(tmplMatches~=0));

end
