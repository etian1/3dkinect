%% Undistorts images given the calibration results from Calib_Results.m.
% This function corrects the lens distortion in a series of images
% contained in a folder with path imgsPath and calibration parameters
% contained in the file calibFile. Undistorted images are outputted to the
% folder with path outPath.

function correctDistortion(calibFile, imgsPath, outPath)

    % open calibration data
    %calibFile = '../calibration/Calib_Results.m';
    run(calibFile);
    
    % create camera parameters
    s = 0;                          % skew
    cameraMtx = [fc(1), 0, 0; s, fc(2), 0; cc(1), cc(2), 1];
    radDistort = kc(1:3);           % radial distortion
    tangDistort = kc(4:5);          % tangential distortion
    cameraParams = cameraParameters('IntrinsicMatrix', cameraMtx, ...
                                    'RadialDistortion', radDistort, ...
                                    'TangentialDistortion', tangDistort);

    % undistort images
    %imgsPath = '../calibration/Image*.pgm';
    imgs = imageDatastore(imgsPath);        % image dataset
    numImgs = length(imgs.Files);           % # calibration images
    
    for i = 1:numImgs
        curr = strjoin(imgs.Files(i));      % name as string
        [~, name, ext] = fileparts(curr);   % filename and extension
        I = imgs.readimage(i);
        J = undistortImage(I, cameraParams);
        outname = strcat(outPath, '/', name, '_undistorted', ext);
        imwrite(J, outname);                % write to .pgm
        
        %{
        % display as a sanity check
        figure;
        imshowpair(I, J, 'montage');
        title('Original Image (left) vs. Corrected Image (right)');
        %}
    end
                                
end