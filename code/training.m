%% Description
% This script uses training images at known distances from either the new
% or old data set to find correspondences between dots in each training
% image with dots in the template training image. The locations of these
% dots that have been matched to dots in the template training image are
% stored in the matrix trainingDots. Each row in training dots represents 
% an image/distance, each column represents a specific dot found within the
% template image, and each entry contains the (x,y) coordinates of the
% relevant dot if a correspondence was identified.

%% Choose data set
dataSet = 'Old';
% dataSet = 'New';

%% Set image paths and distances based on data set
clear trainingDots;
if (strcmp(dataSet,'New'))
    imgDirName = '../training2/';
    dists = [50:5:150] + 11;
    imgNames = ['';''];
    for i=1:length(dists)
        imgNames(i,:) = strcat(imgDirName, sprintf('%03d',dists(i)),'mm.ppm');
    end
    tmplImgName = imgNames(size(imgNames,1),:);

elseif (strcmp(dataSet, 'Old'))
    imgDirName = '../training1/';
    dists = [120:1:190] + 11;
    imgNames = ['';''];
    for i=1:length(dists)
        imgNames(i,:) = strcat(imgDirName, sprintf('%03d',dists(i)),'mm.ppm');
    end
    tmplImgName = imgNames(ceil(size(imgNames,1)/2),:);
end

%% Find dots in template image (one of the training images)
% All other images will have dots matched up with dots in this template
% image
imgTmpl = imread(tmplImgName);
[centersTmpl,~] = getDots(imgTmpl, true, true, false);
imgSize = size(imgTmpl);

%% Find corresponding dots in all other training images
% Find dots in each training image and find correspondences between these
% dots and dots in the template image. Store coordinates of dots that have
% been matched in trainingDots matrix. In trainingDots, the row is the
% image number, the column is the dot index representing which dot in the
% template image a given dot is matched with, and each entry has (x,y)
% coordinates of the centroid of the dot
trainingDots = zeros(size(imgNames,1),size(centersTmpl,1),2); 

for testImgIdx=1:size(imgNames,1)     % For each training image
 
    imgName = imgNames(testImgIdx,:);
    disp(imgName);    % Print image name to track progress while running
    
    % If current image is template, store dot coordinates for all dots
    if (strcmp(imgName, tmplImgName))
        for dotIdx=1:size(centersTmpl,1)
            trainingDots(testImgIdx,dotIdx,:) = centersTmpl(dotIdx,:);
        end
        
    % If not template image, find dot correspondences then store
    % coordinates
    else    
        imgTest = imread(imgName);
        
        % Find dots in new image
        % centersTest has (x,y) coordinates for each dot found in each row
        [centersTest,~] = getDots(imgTest, true, true, false);
        
        % Find matches with dots in template image
        % Index in tmplMatches represents dot index in template image. If
        % there is a match in the current test image with a dot in the
        % template image, tmplMatches will store the dot index of the
        % matching dot in the test image. If there was no match in the
        % current test image for a dot in the template image, tmplMatches
        % will have a 0. 
        tmplMatches = matchDots(imgTmpl, imgTest, centersTmpl, centersTest);

        % Store results in trainingDots
        for dotIdx=1:size(tmplMatches,1)   % For each dot in template image

            % If there was a matching dot in test image
            if (tmplMatches(dotIdx) ~= 0)

                % Get index of matching dot within list of test image dots
                dotIdxTest = tmplMatches(dotIdx);
                
                % Get and store coordinates of matching dot in test image
                trainingDots(testImgIdx, dotIdx,:) = centersTest(dotIdxTest,:);
            end
        end
    
        %% Display matches side by side
        % Comment in the lines below to check that correspondences are good
        
%         % Template image and current test image side-by-side
%         imgComb = horzcat(imgTmpl, imgTest);
%         
%         % Shift coordinates of dot centers in test image by image width
%         centersTestShifted = centersTest;
%         centersTestShifted(:,1) = centersTest(:,1) + imgSize(2);
%         
%         % Display combined image
%         figure;
%         imshow(imgComb);
%         hold on;
%         
%         % For each dot matching, draw line between matching dots in images
%         for dotIdx=1:1:size(tmplMatches,1)
%             dotIdxTest = tmplMatches(dotIdx);
%             if(tmplMatches(dotIdx) ~= 0) % If there is a match
%                 
%                 % Get coordinates of two dots
%                 centerTestX = centersTestShifted(dotIdxTest,1);
%                 centerTestY = centersTestShifted(dotIdxTest,2);
%                 centerTmplX = centersTmpl(dotIdx,1);
%                 centerTmplY = centersTmpl(dotIdx,2);
%                 
%                 % Plot line between dots in images
%                 plot([centerTmplX,centerTestX],[centerTmplY,centerTestY],...
%                     'LineWidth',2,...
%                     'MarkerSize',5);
%             end
%         end
        
        
    end
end

%% Save coordinates of dots in training images
save(strcat('trainingDots', dataSet, '.mat'),'trainingDots','dists','imgNames');

