Shohini Ghosh and Elli Tian
600.456 (CIS2) Project - Software for an Intraoperative Kinect
Spring 2017

Last updated May 14, 2017

How To Use Code for 3D Reconstruction:
1. Run training.m
The data set of training images to use should be selected (either
old data set with images from 131-201mm in 1mm increments and aperture
held constant, or new data set with images from 61-161mm in 5mm increments
with aperture manually adjusted). The appropriate images and distances
will be filled in based on this selection. Then, the script can be run.

If using a new set of training images, make sure to correctly indicate
image file names, the range of distances for these images, and which
training image should be used as the template image. Also, a new suffix
for the .mat filename should be used.

2. Run buildLookupTable.m
The data set of training images should again be specified (use same data
set that was used for running training.m). Then, some parameters can be
adjusted including startDistIdx, polyOrder, fitThresh, plotDim, plotFit
(see code for details on these parameters). Then, the script can be run.

If running with a new set of training images, make sure to update the 
.mat filename suffixes appropriately. 

3. Run makeDepthMap.m
Specify training data set to be used for computing depth map. Select 
input image (using variable testImgName) and use the trainingImg option
if the image is one of the training images (sanity check) and you want
to find the number of "good" and "bad" matches. Adjust parameters such
as rad, windowRad, nccThresh, max1Line, goodThresh as necessary (see
code for details on these parameters). Then, the script can be run.

Make sure to specify a new input image to be used for computing a 3D
point cloud that matches with the data set that is being used!!! The 
best images to use for data set 1 in training1/ are the images in 
testing1/. The best images to use for data set 2 in training2/ are in
testing2/. There are some extra images for data set 1 in 
extraTestingImages1/ but these may not be good testing images. 

3D RECONSTRUCTION SUMMARY
If these three scripts are run in sequence correctly, the third script
will produce and plot a 3D point cloud based on the training images used
and the new input test image specified. 

--------------------------------------------------------------------------

How To Use Code for Finding Dots:
1. MSER: getDots.m
This code finds MSER features, dot pattern center coordinates, and/or a list
of pixel coordinates for each feature in an image I, represented as an
array. Booleans are also passed into the function to indicate whether or not
a center of the pattern should be found, if duplicates (i.e. dots that are
within some minimum distance of each other) should be eliminated, and if
a list of pixel coordinates for each feature should be returned.

By default, a matrix with a list of the (x,y) coordinates of the centers is
returned and the number of centers found is printed to the command line. The
following lines can be uncommented for additional features:

- lines 48-49, to show the number of MSER regions detected
- line 83, to plot the center of the laser pattern
- lines 142-146, to show the MSER regions overlayed on the image

2. SIFT: getDotsSIFT.m
This code finds SIFT keypoints and descriptors in an image I, inputted as
an image file. The features are further refined by thresholding with a peak
selection threshold, non-edge selection threshold, a maximum pixel radius, 
and green amount (represented with RGB color values). These parameters
can be adjusted on lines 27, 30, 41, and 45. In addition, lines 39-47 can
be commented accordingly to threshold feature size with an absolute or a
relative cutoff. The code plots the SIFT features on the image, and returns
the keypoints and descriptors.
